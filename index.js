// [section] Comments

// Comments are parts of the code that gets ignored by the language.
// Comments are meant to describe the written code.

// There are two types of comments:
// -Single line comment (ctrl + /) denoted by two slashes (//)

/*

    - multiline comment
    - denoted by (/**)/
    - ctrl + shift + /

*/


// [Section] Statements and Syntax
    // Statements
       // In programming language, Statements
       // are instruction that we tell the computer
       // to perform.
       // JS statement usually ends with semicolon(;)
       // (;) is also called as delimeter.
       // Semicolons are not required.
          // it use to help us train or locate where a state ends.

     //Syntax 
       // In programming, it is the set of rules that describes
       // how statements must be constructed.

console.log("Hellow World");

 // [Section] Variables 
 // It is used to contain data.
   // Declaring a variable
     //tell our devices that a variable name
     // is created and is ready to store data.


     // Syntax : let/const variableName;

     // let is a keyword that is usually used in 
     // declaring a variable.

       let myVariable;

       console.log(myVariable); //useful for printing values of a variable
       // console.log(Hello); //error: not defined
       //Initialize a value/Initialization a value
          // Storing the initial value of a variable.
          // Assignment Operator (=)

          myVariable = "Hello";
           //Reassignment a variable value
             //Changing the initial value to a new value.
          myVariable ="Hello World"

          console.log(myVariable);

     // const 
         //"const" keywordd is used to declare and initialized a
           //constant variable.
           //a "constant" variable does not change.

     // const PI;
     // PI = 3.1416;
     // console.log(PI); //error: Initial value is missing for const.

     //Declaring with Initialization 
         // a variable is given its initial/starting upon
         //declaration.
            //Syntax: let/const variableName = value;
             
            let productName ="Desktop Computer";
            console.log(productName);

            let productPrice = 18999;
            // let productPrice = 20000; error: variable has been declared.
            productPrice = 20000;
            console.log(productPrice);

            //const keyword

            const PI = 3.1416;
           // PI = 3.14; error: assignment to constant variable.
            console.log(PI);

            /*
              Guide in writing variables.

              1. Use the "let" keyword if the variables will contain
              different values/can change over time.
                 Naming Convention: variables name should starts with
                 lowercase characters and camelCasing is use for multiple words.
               
              2. Use the "const" keyword if the variable does not changed.
                 Naming Convention: "const" keyword should be followed
                 by ALL CAPITAL VARIABLE NAME (Single value variable)

              3. Variable names should be indicative (or descriptive) of 
              the value being stored to avoid confusion.

              Avoid this one: let word = "John Smith"

              4. Variable name are case sensitive.

            */

              //Multiple variable declaration

                let productCode = "DC017" , productBrand = "Dell";
                console.log(productCode, productBrand);

            // Using a reserve keyword
                //const let = "hello";
                //console.log(let); erorr; lexically boud is disallowed.


        // [SECTION] Data Types 

        // In JavaScript there are six types of data

             //String
               // are series of characters that create a word, a 
               //phrase, a sentence or anything related to creating text.
               // Strings in JavaScript can be written using a single quote('')
               // or double quote("")

               let country = "philippines";
               let province = 'Metro Manila';

               //concatenating Strings
               // Multiple string values that can be combined to
               // create a single String using the "+" symbol.
              // I live in Metro Manila, Philippines
               let greeting = " I live in " + province + ", " + country;

               console.log(greeting);


               //Escape Characters
               //(\) in String combination with other characters can produce different 
               //result
               //"\n" refers to creating a new line.
                     
               let mailAddress = "Quezon City \n philippines";
               console.log(mailAddress);


               //Expected output: John's employees went home early

               let message = "John's employees whent home early.";
               console.log(message);
                // using the escape characters (/);
               message = 'Jane\'s employees whent home early.';
               console.log(message);

               //Numbers
                    // includes positive, negative numbers
                   // Integers/Whole Numbers
                   let headcount = 26;
                   console.log(headcount);


                   // Decimal Numbers/Fractions

                   let grade = 98.7;
                   console.log(grade);

                   //Exponential Notation
                   let planetDistance = 2e10;
                   console.log(planetDistance);

                   //combine text and strings 
                   console.log("John's grade last quarter is " + grade);

                   // Boolean
                     //Boolean values are normally used to store values
                     // relating to the state of certain things.
                     // true or false (two logical values)

                     let isMarried = false;
                     let isGoodCunduct = true;

                     console.log("isMarried "+ isMarried);
                     console.log("isGoodCunduct "+ isGoodCunduct);

                  //Arrays
                     // are special kind of data type that can used to store
                     // multiple related values.
                      // Syntax: let/const arrayName = [elementA, elementB,... 
                      //elementNth]

                      const grades = [98.7, 92.1, 90.2, 94.6];
                      //const variable cannot be reassigned.
                      //grades = 100; // not allowed

                      // Changing the elements of an array or changing the 
                      //properties of an boject is allowed in constant variable
                      grades[0] = 100;//reference value/index value
                      console.log(grades);


                       //this will work, but not recommended.
                      let details = ["John", "Smith", 32, true];
                      console.log(details);

                //Objects
                  // Objects are another special kind of data type that is 
                  //is used to mimic real world objects/items.
                  //used to create complex data that contains information
                  //relevant to each other.  

                  /*

                      Syntax:
                      let/ const objectName = {
    
                           propertyA: valueA,
                           propertyB: valueB
                      }

                  */ 

                  let person = {
                    fullName: "Juan Dela Cruz",
                    age: 35,
                    isMarried:false,
                    contact: ["+63912 345 6789", "8123 456"],
                    address: {
                        houseNumber: "345",
                        city: "Manila"
                    }
                  };  

                  console.log(person);

                  // They're are also useful for creating abstract
                  //objects
                  let myGrades = {
                    firstGrading: 98.7,
                    secondingGrading: 92.1,
                    thirdGrading: 90.2,
                    fourthGrading: 94.6
                  }

                  console.log(myGrades);

                  //typeof operator is used to determined the type of 
                  // data or the value of a variable.

                  console.log(typeof myGrades); //object

                  //array is a special type of object with methods
                  console.log(typeof grades);//object


            //null 
              // indicates the absence of value.

              let spouse = null;

              console.log(spouse);

            //undefined
              //indicates that a variables has not been given a 
                //value yet.

                let fullName;
                console.log(fullName)
